import fetch from "node-fetch";
import AbortController from 'abort-controller';
import fs from "fs";

async function parse_category({name, channels}) {
    const parsed_channels = await Promise.all(channels.map(parse_channel));

    return {
        category: name,
        videos: parsed_channels.filter(is_channel_valid),
    };
}

async function putInObject(videos){

    return {
        googlevideos: videos,
    };

}

const filter_async = async (arr, predicate) => {
    const results = await Promise.all(arr.map(predicate));
    return arr.filter((_v, index) => results[index]);
}

async function parse_channel({web,logo,name, epg_id, options}, num) {
        const urls = await filter_async(options.map(({url}) => url), is_source_valid);
        return {
            description: web,
            card: logo,
            background: logo,
            title: name , // shorthand propery, https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#new_notations_in_ecmascript_2015
            studio: epg_id ?? name, // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator
            //sources: options.map(({url}) => url),
            sources: urls,
        };
}

function is_empty(array) {
    return array.length === 0;
}


async function is_source_valid(url) {
    // do stuff here

    if(url.endsWith('.m3u8')) {
        try {
            const req = await fetch(url, {method: "HEAD", signal: ac.signal});
            return req.status === 200;
        } catch {}
    }
    return false;
}

function is_channel_valid({sources}) {
    return !is_empty(sources);
}

function is_category_valid({videos}) {
    return !is_empty(videos);
}


async function download_text(url) {
    const req = await fetch(url);
    return await req.text();
}



async function download_and_convert() {
    const url = "https://www.tdtchannels.com/lists/tv.json";
    const text = await download_text(url);
    return JSON.parse(text);
}

async function sleep(ms) {
    return new Promise((res) => {
        setTimeout(res, ms)
    })
}

const ac = new AbortController();

async function main(){
    const info = await download_and_convert();
    const categories = info.countries.flatMap(({ambits}) => ambits);

    sleep(5000).then(() => {
        ac.abort();
    })
    const parsed_categories = await Promise.all(categories.map(parse_category));
    const filtered_categories = parsed_categories.filter(is_category_valid);


    const json = JSON.stringify({"googlevideos":filtered_categories});
    fs.writeFileSync("television.json", json);
}



await main();
